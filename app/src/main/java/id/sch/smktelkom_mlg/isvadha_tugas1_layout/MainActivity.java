package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.FavoriteAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.RecyclerViewAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item.TabItem_RecyclerView_Favorite;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item.TabItem_RecyclerView_Horizontal;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item.TabItem_RecyclerView_Korea;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item.TabItem_RecyclerView_Maps;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item.TabItem_RecyclerView_Vertical;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.FoodModel;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.ShowDataDetail;


public class MainActivity extends AppCompatActivity implements RecyclerFood.mClickListener, FavoriteAdapter.MyListener   {

    //implements RecyclerViewAdapter.mClickListener

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private int[] tabIcons = {
            R.drawable.ic_vertical_align_bottom_black_24dp,
            R.drawable.ic_swap_horiz_black_24dp,
            R.drawable.ic_cake_black_24dp,
            R.drawable.ic_white_favorite_24dp,
            R.drawable.ic_location_on_black_24dp
    };

//    private int[] tabIcons = {
//            R.drawable.ic_favorite_black_24dp
//    };

    private String[] setTitle = {"Vertical", "Horizontal", "Meal", "Favorite", "Maps"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.main_viewpager);
        setupWithViewPager(viewPager);

        tabLayout = findViewById(R.id.main_tablayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getSupportActionBar().setTitle(setTitle[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabIcons();

    }

    private void setupTabIcons() {
        getSupportActionBar().setTitle(setTitle[0]);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }

    private void setupWithViewPager(ViewPager viewPager) {
        //Initial TabLayout Adapter
        TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getSupportFragmentManager());

        tabLayoutAdapter.AddFragmentPage(new TabItem_RecyclerView_Vertical(), "VRT");
        tabLayoutAdapter.AddFragmentPage(new TabItem_RecyclerView_Horizontal(), "HRZ");
        tabLayoutAdapter.AddFragmentPage(new TabItem_RecyclerView_Korea(), "MEAL");
        tabLayoutAdapter.AddFragmentPage(new TabItem_RecyclerView_Favorite(), "FAV");
        tabLayoutAdapter.AddFragmentPage(new MapsActivity(), "MAP");
//        tabLayoutAdapter.AddFragmentPage(new MapsActivity(), "Maps");

        viewPager.setAdapter(tabLayoutAdapter);
    }

    class TabLayoutAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragment = new ArrayList<>();
        private List<String> mTitle = new ArrayList<>();

        public TabLayoutAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragment.get(position);
        }

        @Override
        public int getCount() {
            return mTitle.size();
        }

        public void AddFragmentPage(Fragment fragment, String title) {
            mFragment.add(fragment);
            mTitle.add(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_save){
            Intent userProfile = new Intent(MainActivity.this,UserActivity.class);
            startActivity(userProfile);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void mOnClickListener(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        Intent intent = new Intent(MainActivity.this, ShowDataDetail.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void mOnClickListenerFavorite(FoodModel model) {

        Bundle bundle = new Bundle();
        bundle.putInt("Id", model.getId());
        bundle.putInt("IdMakanan", model.getId_makanan());
        bundle.putString("StrGambarMakanan", model.getGambar_makanan());
        bundle.putString("StrNamaMakanan", model.getNama_makanan());
        bundle.putString("StrKategori", model.getKategori());
        bundle.putString("StrArea", model.getAsal_negara());
        bundle.putString("StrYoutube", model.getLink_youtube());
        bundle.putString("StrWeb", model.getLink_web());
//        bundle.putString("StrInstruksi", model.getInstruksi_pembuatan());
        Intent i = new Intent(MainActivity.this, ShowDataDetail.class);
        i.putExtras(bundle);
        startActivity(i);

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }


}
