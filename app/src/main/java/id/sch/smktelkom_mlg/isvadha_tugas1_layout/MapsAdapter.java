package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MapsAdapter extends RecyclerView.Adapter<MapsAdapter.ViewHolder>  {

    List<MapsModel> mapsModelArrayList;
    MapsHelper realmHelper;
    Realm realm;

    public MapsAdapter(List<MapsModel> mapsModelArrayList) {

        this.mapsModelArrayList = mapsModelArrayList;

    }

    @NonNull
    @Override
    public MapsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.maps_list, viewGroup, false));
    }

//    @Override
//    public void onBindViewHolder(@NonNull final MapsAdapter.ViewHolder viewHolder, int i) {
//
//        viewHolder.img.setImageURI(Uri.parse(mapsModelArrayList.get(i).getGambar_lokasi()));
//        viewHolder.txt.setText(mapsModelArrayList.get(i).getNama_lokasi());
//        viewHolder.delet.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                realmHelper.delete("nama_lokasi",mapsModelArrayList.get(i).getNama_lokasi());
//                Toast.makeText(viewHolder.itemView.getContext(), "Swipe to update", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//    }

    @Override
    public void onBindViewHolder(@NonNull final MapsAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.img.setImageURI(Uri.parse(mapsModelArrayList.get(i).getGambar_lokasi()));
        viewHolder.txt.setText(mapsModelArrayList.get(i).getNama_lokasi());
        viewHolder.delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realmHelper.delete("nama_lokasi",mapsModelArrayList.get(i).getNama_lokasi());
                Toast.makeText(viewHolder.itemView.getContext(), "Swipe up untuk melihat data terbaru", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mapsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView txt;
        Button delet;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
            realm = Realm.getInstance(realmConfiguration);
            realmHelper = new MapsHelper(realm);

            img = itemView.findViewById(R.id.imageMaps);
            txt = itemView.findViewById(R.id.textMaps);
            delet = itemView.findViewById(R.id.btnDeleteMaps);


        }
    }
}
