package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.CategoryRecyclerAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.RealmHelper;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.MapsAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.MapsHelper;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.MapsModel;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Category;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Items;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Movie;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.RecyclerFood;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Rest.ApiClient;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Rest.ApiInterface;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabItem_RecyclerView_Korea extends Fragment {

    MapsHelper realmHelper;
    public View view;
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView, recyclerViewCategori, rvMaps;
    private MoviesAdapter mAdapter;

    RecyclerFood.mClickListener mClickListener;

    public TabItem_RecyclerView_Korea() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mClickListener = (RecyclerFood.mClickListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tabitem_recyclerview_korea, container, false);

        recyclerView = view.findViewById(R.id.ID_tabitem_recyclerview_korea);
        recyclerViewCategori = view.findViewById(R.id.ItemKategori);

        rvMaps = view.findViewById(R.id.recycler_view_maps);


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Items> foodcall = apiInterface.getData();
        foodcall.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                Log.wtf("Data", response.body().getMeal().get(0).getNamaMakanan());
                setDataRecycler(response.body().getMeal());

            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {
                Log.wtf("hasilWithparam", t.getMessage());
            }
        });

        recyclerViewCategori = view.findViewById(R.id.ID_tabitem_recyclerview_korea_categori);

        ApiInterface apiInterfaceKategori = ApiClient.getClient().create(ApiInterface.class);
        Call<Category> categoryCall = apiInterfaceKategori.getKategori();
        categoryCall.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                Log.wtf("Kategori", response.body().getCatogries().get(0).getStrCategory());
                recyclerViewCategori.setHasFixedSize(false);
                recyclerViewCategori.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
                recyclerViewCategori.setAdapter(new CategoryRecyclerAdapter(response));
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {

            }
        });

//        recyclerView.addOnItemTouchListener(new RecyclerItTouchListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                final int in = position;
//                //Toast.makeText(getContext(), mListm.get(position).getStrMeal(), Toast.LENGTH_SHORT).show();
//                Snackbar snackbar = Snackbar
//                        .make(view, mListm.get(in).getStrMeal(), Snackbar.LENGTH_LONG)
//                        .setAction("Add to Favorite", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                //Toast.makeText(getContext(), mListm.get(in).getStrMeal(), Toast.LENGTH_SHORT).show();
//                                //Toast.makeText(getContext(), mListm.get(in).getIdMeal().toString(), Toast.LENGTH_SHORT).show();
//
//                                //simpan data
//                                makanan = new Makanan();
//                                makanan.setIdMeal(mListm.get(in).getIdMeal());
//                                makanan.setStrMeal(mListm.get(in).getStrMeal().toString());
//                                makanan.setStrMealThumb(mListm.get(in).getStrMealThumb().toString());
//
//                                try {
//                                    realmHelper = new RealmHelper(realm);
//                                    realmHelper.save(makanan);
//                                    Toast.makeText(getContext(), "Meal added", Toast.LENGTH_SHORT).show();
//                                }catch (Exception e){
//                                    Toast.makeText(getContext(), "Meal already added before", Toast.LENGTH_SHORT).show();
//                                }
//
//                                //get data
////                                makananMakanan = realmHelper.getAllMakanan();
////                                for (Makanan m:makananMakanan) {
////                                    Toast.makeText(getContext(), m.getStrMeal().toString(), Toast.LENGTH_SHORT).show();
////                                }
//
//
//
//                                //delete data
//                                //realmHelper.delete(1);
//
//
//                            }
//                        });
//
//                snackbar.show();
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                // ...
//            }
//        }));
//
        /*mAdapter = new MoviesAdapter(movieList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Movie movie = movieList.get(position);
                Toast.makeText(getActivity(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareMovieData();*/
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(realmConfiguration);
        realmHelper = new MapsHelper(realm);
        setRvMaps();


        return view;
    }

    private void setDataRecycler(ArrayList<Items.Meals> mListener){
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new RecyclerFood(mListener, this.mClickListener));
    }



    public void setRvMaps() {
        List<MapsModel> mapsModelArrayList = realmHelper.getAllData();
        rvMaps.setHasFixedSize(false);
        rvMaps.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        rvMaps.setAdapter(new MapsAdapter(mapsModelArrayList));
    }


/*
    private void prepareMovieData() {
        Movie movie = new Movie("Hotel Tugu", "Hotel", "**");
        movieList.add(movie);

        movie = new Movie("Atap Rumah", "Atap", "*");
        movieList.add(movie);

        movie = new Movie("Jembatan Layang", "Jembatan", "*****");
        movieList.add(movie);

        movie = new Movie("China", "China", "**");                                        gggggggg
        movieList.add(movie);

        movie = new Movie("Masjid Al-Bashirah", "Masjid", "*");
        movieList.add(movie);

        movie = new Movie("Gunung Bromo", "Gunung", "****");
        movieList.add(movie);

        movie = new Movie("Gunung Panderman", "Gunung", "**");
        movieList.add(movie);
        movie = new Movie("Gunung Semeru", "Gunung", "*");
        movieList.add(movie);
        movie = new Movie("Gunung Kelud", "Gunung", "***");
        movieList.add(movie);
        movie = new Movie("Gunung Kawi", "Gunung", "**");
        movieList.add(movie);


        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }*/

}