package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.ItemList;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.RecyclerViewAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;

public class TabItem_RecyclerView_Horizontal extends Fragment {

    public View view;
    private RecyclerView mRecyclerView;

    private ArrayList<ItemList> mItemList = new ArrayList<>();
    private RecyclerViewAdapter mRecyclerViewAdapter;

    public TabItem_RecyclerView_Horizontal() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tabitem_recyclerview_horizontal, container, false);

        mRecyclerView = view.findViewById(R.id.ID_tabitem_recyclerview_horizontal);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerViewAdapter = new RecyclerViewAdapter(getContext(), mItemList, "horizontal");
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        DoBindData();

        return view;
    }

    private void DoBindData() {
        Resources resources = getResources();
        String[] Title = resources.getStringArray(R.array.tittle);
        TypedArray a = resources.obtainTypedArray(R.array.gambar);
        Drawable[] Gambar = new Drawable[a.length()];

        for (int i = 0; i < Gambar.length; i++) {
            Gambar[i] = a.getDrawable(i);
        }
        for (int i = 0; i < Title.length; i++) {
            mItemList.add(new ItemList(Gambar[i], Title[i]));
        }

        mRecyclerViewAdapter.notifyDataSetChanged();
    }

}