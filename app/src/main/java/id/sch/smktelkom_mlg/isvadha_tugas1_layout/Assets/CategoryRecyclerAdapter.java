package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Category;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;
import retrofit2.Response;

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.ViewHolder> {
    Response<Category> mData;

    public CategoryRecyclerAdapter(Response<Category> response) {
        this.mData = response;
    }

    @NonNull
    @Override
    public CategoryRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryRecyclerAdapter.ViewHolder viewHolder, int i) {
        viewHolder.setData(mData.body().getCatogries().get(i).getStrCategory(), mData.body().getCatogries().get(i).getStrCategoryThumb());
    }

    @Override
    public int getItemCount() {
        return mData.body().getCatogries().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.foto_kategori);
            textView = itemView.findViewById(R.id.nama_kategori);
//            CardView cardViewCategory = itemView.findViewById(R.id.ItemKategori);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
//                }
//            });
//            if (textView == null) {
//                Toast.makeText(itemView.getContext(), "Pastikan Jaringan Anda Lancar", Toast.LENGTH_SHORT).show();
//                cardViewCategory.setVisibility(View.GONE);
//            } else {
//                cardViewCategory.setVisibility(View.VISIBLE);
//            }
        }

        public void setData(String title, String img) {
            textView.setText(title);
            Picasso.get().load(img).into(imageView);
        }
    }
}

