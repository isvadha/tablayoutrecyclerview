package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    public Context context;
    public ArrayList<ItemList> itemLists;
    public String Code;

    public RecyclerViewAdapter(Context context, ArrayList<ItemList> berandaLists, String code) {
        this.context = context;
        this.itemLists = berandaLists;
        this.Code = code;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        int id;

        if (Code.equals("vertical")) {
            id = R.layout.item_recyclerview_vertical;
        } else if (Code.equals("horizontal")) {
            id = R.layout.item_recyclerview_horizontal;
        } else if (Code.equals("korea")) {
            id = R.layout.item_recyclerview_korea;
        } else {
            id = R.layout.item_recyclerview_vertical;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(id, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {

        ItemList mItemLists = itemLists.get(position);

        holder.Logo.setBackground(mItemLists.Gambar);
        holder.Tittle.setText(mItemLists.Text);
    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout Logo;
        private TextView Tittle;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            Logo = itemView.findViewById(R.id.Logo_Item);
            Tittle = itemView.findViewById(R.id.Tittle_Item);
        }
    }
}
