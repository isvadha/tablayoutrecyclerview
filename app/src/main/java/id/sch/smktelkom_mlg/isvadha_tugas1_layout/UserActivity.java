package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class UserActivity extends AppCompatActivity {

    EditText nama, email, alamat;
    Button save_data , edit_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profil);

        nama = findViewById(R.id.nama);
        email = findViewById(R.id.email);
        alamat = findViewById(R.id.alamat);
        save_data = findViewById(R.id.save_data);
        edit_data = findViewById(R.id.edit_data);


        final String data = Preferences.getNama(getApplicationContext());
        final String dataEmail = Preferences.getEmail(getApplicationContext());
        final String dataAlamat = Preferences.getAlamat(getApplicationContext());

        nama.setText(data);
        alamat.setText(dataAlamat);
        email.setText(dataEmail);
        if (!nama.getText().toString().equals("") || !alamat.getText().toString().equals("") || !email.getText().toString().equals("")) {
            nama.setEnabled(false);
            alamat.setEnabled(false);
            email.setEnabled(false);

            save_data.setVisibility(View.GONE);

            edit_data.setVisibility(View.VISIBLE);
        }

        save_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setNama(getApplicationContext(), nama.getText().toString());
                Preferences.setEmail(getApplicationContext(), email.getText().toString());
                Preferences.setAlamat(getApplicationContext(), alamat.getText().toString());

                if (!nama.getText().toString().equals("") || !alamat.getText().toString().equals("") || !email.getText().toString().equals("")) {
                    nama.setText(data);
                    email.setText(dataEmail);
                    alamat.setText(dataAlamat);
                }

                nama.setEnabled(false);
                alamat.setEnabled(false);
                email.setEnabled(false);

                save_data.setVisibility(View.GONE);

                edit_data.setVisibility(View.VISIBLE);

                Intent i = new Intent(UserActivity.this, UserActivity.class);
                startActivity(i);
                finish();

            }
        });

        edit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nama.setEnabled(true);
                alamat.setEnabled(true);
                email.setEnabled(true);

                save_data.setVisibility(View.VISIBLE);

                edit_data.setVisibility(View.GONE);

            }
        });

//
//        save_data.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Preferences.setNama(getApplicationContext(),nama.getText().toString());
//                Preferences.setEmail(getApplicationContext(),email.getText().toString());
//                Preferences.setAlamat(getApplicationContext(),alamat.getText().toString());
//                if (!nama.getText().toString().equals("") && !email.getText().toString().equals("") && !alamat.getText().toString().equals("")){
//                    Toast.makeText(UserActivity.this, "Save data success!!!!!!!", Toast.LENGTH_SHORT).show();
//                }
//                nama.setText("");
//                alamat.setText("");
//                email.setText("");
//            }
//        });

//        show_data.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String show_nama = Preferences.getNama(getApplicationContext());
//                String show_email = Preferences.getEmail(getApplicationContext());
//                String show_alamat = Preferences.getAlamat(getApplicationContext());
//
//                nama.setText(show_nama);
//                email.setText(show_email);
//                alamat.setText(show_alamat);
//            }
//        });
//
//        delete_data.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Preferences.deleteNama(getApplicationContext());
//                Preferences.deleteEmail(getApplicationContext());
//                Preferences.deleteAlamat(getApplicationContext());
//                Toast.makeText(UserActivity.this, "data deleted!!!!!!!", Toast.LENGTH_SHORT).show();
//            }
//        });

    }
}
