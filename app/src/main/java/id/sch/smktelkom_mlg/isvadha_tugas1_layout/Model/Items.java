package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items {

    @SerializedName("meals")
    private ArrayList<Meals> meals;

    public ArrayList<Meals> getMeal() {
        return meals;
    }

    public class Meals {
        @SerializedName("idMeal")
        private int idMeal;

        @SerializedName("strMealThumb")
        private String gambar;

        @SerializedName("strMeal")
        private String namaMakanan;

        @SerializedName("strCategory")
        private String strCategory;

        @SerializedName("strTags")
        private String strTags;

        @SerializedName("strInstructions")
        private String strInstructions;

        @SerializedName("strArea")
        private String area;

        @SerializedName("strSource")
        private String source;

        @SerializedName("strYoutube")
        private String youtube;

        public int getIdMeal() {
            return idMeal;
        }

        public String getGambar() {
            return gambar;
        }

        public String getNamaMakanan() {
            return namaMakanan;
        }

        public String getStrCategory() {
            return strCategory;
        }

        public String getStrTags() {
            return strTags;
        }

        public String getStrInstructions() {
            return strInstructions;
        }

        public String getArea() {
            return area;
        }

        public String getSource() {
            return source;
        }

        public String getYoutube() {
            return youtube;
        }
    }
}
