package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    static final String KEY_NAMA ="Nama";
    static final String KEY_EMAIL ="Email";
    static final String KEY_ALAMAT ="Alamat";

    /** Pendlakarasian Shared Preferences yang berdasarkan paramater context */
    public static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    //data = parameter
    //void karena tdk me return
    public static void setNama(Context context, String nama){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_NAMA, nama);
        editor.apply();
    }
    public static void setEmail(Context context, String email){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }
    public static void setAlamat(Context context, String alamat){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_ALAMAT, alamat);
        editor.apply();
    }

    public static String getNama(Context context){
        return getSharedPreference(context).getString(KEY_NAMA,"");
        //getdata tdk ada string data karena cuma buat ngambil data, tdk butuh masukan / inputan
    }
    public static String getEmail(Context context){
        return getSharedPreference(context).getString(KEY_EMAIL,"");
        //getdata tdk ada string data karena cuma buat ngambil data, tdk butuh masukan / inputan
    }
    public static String getAlamat(Context context){
        return getSharedPreference(context).getString(KEY_ALAMAT,"");
        //getdata tdk ada string data karena cuma buat ngambil data, tdk butuh masukan / inputan
    }


    public static void deleteNama (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_NAMA);
        editor.apply();
    }
    public static void deleteEmail (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_EMAIL);
        editor.apply();
    }
    public static void deleteAlamat (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_ALAMAT);
        editor.apply();
    }
}

