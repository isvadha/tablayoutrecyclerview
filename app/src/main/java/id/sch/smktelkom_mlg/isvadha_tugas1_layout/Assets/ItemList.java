package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets;

import android.graphics.drawable.Drawable;

public class ItemList {
    public Drawable Gambar;
    public String Text;

    public ItemList(Drawable gambar, String text) {
        Gambar = gambar;
        Text = text;
    }
}