package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MapsHelper {

    Realm realm;

    public MapsHelper(Realm realm) {
        this.realm = realm;
    }

    // untuk menyimpan data
    public void save(final MapsModel mapsModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(MapsModel.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    mapsModel.setId(nextId);
                    MapsModel model = realm.copyToRealm(mapsModel);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }

    // untuk memanggil semua data
    public List<MapsModel> getAllData() {
        List<MapsModel> results = realm.where(MapsModel.class).findAll();
        return results;
    }

    // untuk meng-update data
    public void update(final Integer id, final String latitude_lokasi, final String gambar_lokasi, final Double latitude, final Double longitude) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MapsModel model = realm.where(MapsModel.class)
                        .equalTo("id", id)
                        .findFirst();
                model.setNama_lokasi(latitude_lokasi);
                model.setGambar_lokasi(gambar_lokasi);
                model.setLatitude_lokasi(latitude);
                model.setLongitude_lokasi(longitude);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("", "onSuccess: Update Successfully");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });
    }


    // untuk menghapus data
//    public void delete(Integer id) {
//        final RealmResults<MapsModel> model = realm.where(MapsModel.class).equalTo("id", id).findAll();
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                model.deleteFromRealm(0);
//            }
//        });
//    }

    public void delete(String kolom, String nama) {
        final RealmResults<MapsModel> model = realm.where(MapsModel.class).equalTo(kolom, nama).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                model.deleteFromRealm(0);
            }
        });
    }


}
