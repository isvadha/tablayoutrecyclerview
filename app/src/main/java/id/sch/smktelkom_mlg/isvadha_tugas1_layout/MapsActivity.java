package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class MapsActivity extends Fragment  implements OnMapReadyCallback, LocationListener {
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    public static final int REQUES_IMAGE = 100;
    Button show, showWe, addLocation;
    LatLng sydney, locSaya;
    TextInputEditText nama_loc, lat_loc, long_loc;
    public static final int REQUES_PERMISSION = 200;
    Realm realm;
    MapsHelper realmHelper;
    MapsModel model;
    double global_latitude, global_longitude;
    private GoogleMap mMap;
    private LocationManager locationManager;
    Marker mSydned, mAdd, mClick, mMe;
    ImageView takePicture;
    private String imageFilePath = "";

    private Boolean init = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_maps, container, false);

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);

        }

        /********** get Gps location service LocationManager object ***********/
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 10, this);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /********* After registration onLocationChanged method  ********/
        /********* called periodically after each 3 sec ***********/

        addLocation = view.findViewById(R.id.addLocationMe);
        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayInput();
            }
        });

        Realm.init(getContext());
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new MapsHelper(realm);

        show = view.findViewById(R.id.btnShow);
        showWe = view.findViewById(R.id.btnShowWe);

        return view;
    }

    private void displayInput() {
        final Dialog d = new Dialog(getContext());
        d.setTitle("Add Location");
        d.setContentView(R.layout.input_layout);

        Button save = d.findViewById(R.id.btnSaveLoc);
        nama_loc = d.findViewById(R.id.nama_tempat);
        lat_loc = d.findViewById(R.id.Latitude);
        long_loc = d.findViewById(R.id.Longtitude);
        takePicture = d.findViewById(R.id.takePicture);

        lat_loc.setEnabled(false);
        long_loc.setEnabled(false);

        final String lati = String.valueOf(Double.valueOf(global_latitude));
        lat_loc.setText(lati);
        final String longi = String.valueOf(Double.valueOf(global_longitude));
        long_loc.setText(longi);

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    }, REQUES_PERMISSION);
                } else {
                    openCameraIntent();
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    model = new MapsModel();
                    model.setNama_lokasi(String.valueOf(nama_loc.getText()));
                    model.setGambar_lokasi(imageFilePath);
                    model.setLatitude_lokasi(Double.valueOf(lati));
                    model.setLongitude_lokasi(Double.valueOf(longi));

                    realmHelper = new MapsHelper(realm);
                    realmHelper.save(model);
                    Toast.makeText(getContext(), "Tertambahkan", Toast.LENGTH_SHORT).show();

                    d.hide();

                } catch (Exception e) {

                }

            }
        });

        d.show();
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            File photoFile = null;

            try {
                photoFile = creatImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            Uri photoUri = FileProvider.getUriForFile(getContext(), getActivity().getPackageName() + ".provider", photoFile);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(pictureIntent, REQUES_IMAGE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUES_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Terimakasih Telah diBeri Akses", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUES_IMAGE) {
            if (resultCode == RESULT_OK) {
                takePicture.setImageURI(Uri.parse(imageFilePath));
                Log.d("FilePath", imageFilePath);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getContext(), "Kamu membatalkan operasi", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File creatImageFile() throws IOException {
        File directory = new File(Environment.getExternalStorageDirectory() + "/" + "file_gambar_tugas");
        if (!directory.exists()) {
            directory.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        /*File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);*/
        File image = File.createTempFile(imageFileName, ".jpg", directory);
        imageFilePath = image.getAbsolutePath();

        return image;
    }

    @Override
    public void onResume() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
        } else {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);
        }

        if (!init) {
            loadData();
        }

        super.onResume();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        InfoWindowCustom customInfoWindow = new InfoWindowCustom(getContext());
        mMap.setInfoWindowAdapter(customInfoWindow);

        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);

        // Add a marker in Sydney and move the camera
        sydney = new LatLng(-34, 151);

        mSydned = mMap.addMarker(new MarkerOptions()
                .position(sydney)
                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_android))
                .title("Marker in Sydney"));

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        final Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(-34, 151), new LatLng(40.7, -74.0))
                .width(5)
                .color(Color.BLUE));

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
            }
        });

        final String coba = "coba";

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                /*
                MarkerOptions marker = new MarkerOptions()
                        .title(coba)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .position(new LatLng(latLng.latitude, latLng.longitude));
                mClick = mMap.addMarker(marker);
*/
                Toast.makeText(getContext(), latLng.latitude + ", " + latLng.longitude, Toast.LENGTH_SHORT).show();
            }
        });

        loadData();

    }

    private void loadData() {
        List<MapsModel> models = realmHelper.getAllData();

        for (MapsModel mapsModel : models) {

            InfoWindowData info = new InfoWindowData();
            info.setImage(mapsModel.getGambar_lokasi());
            info.setFood(mapsModel.getNama_lokasi());
            info.setHotel(String.valueOf(mapsModel.getLatitude_lokasi() + ", " + String.valueOf(mapsModel.getLongitude_lokasi())));

            MarkerOptions marker = new MarkerOptions()
                    .title(mapsModel.getNama_lokasi())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    .position(new LatLng(mapsModel.getLatitude_lokasi(), mapsModel.getLongitude_lokasi()));
            mAdd = mMap.addMarker(marker);
            mAdd.setTag(info);
            mAdd.showInfoWindow();

            mMap.setInfoWindowAdapter(new InfoWindowCustom(getContext()));

            Toast.makeText(getContext(), models.get(0).getNama_lokasi() + "\n" + mapsModel.getLatitude_lokasi() + ", " + mapsModel.getLongitude_lokasi(), Toast.LENGTH_SHORT).show();

        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onLocationChanged(Location location) {

        if (mMe != null) {

            mMe.remove();

        }

        String str = "Latitude: " + location.getLatitude() + "Longitude: " + location.getLongitude();

        global_latitude = location.getLatitude();
        global_longitude = location.getLongitude();

        InfoWindowData info = new InfoWindowData();
        info.setImage("ic_action_name");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");

        MarkerOptions marker = new MarkerOptions().title(" here").icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_android)).position(new LatLng(global_latitude, global_longitude));
        mMe = mMap.addMarker(marker);
        mMe.setTag(info);

        locSaya = new LatLng(global_latitude, global_longitude);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(locSaya));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

        showWe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMap.moveCamera(CameraUpdateFactory.newLatLng(locSaya));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

            }
        });

        Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

        /******** Called when User on Gps  *********/

        Toast.makeText(getContext(), "Gps turned on ", Toast.LENGTH_LONG).show();


    }

    @Override
    public void onProviderDisabled(String provider) {

        /******** Called when User off Gps *********/

        Toast.makeText(getContext(), "Gps turned off ", Toast.LENGTH_LONG).show();


    }


}