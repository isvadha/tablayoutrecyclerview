package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets;

import android.util.Log;

import java.util.List;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.FoodModel;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {

    Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    // untuk menyimpan data
    public void save(final FoodModel mahasiswaModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(FoodModel.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    mahasiswaModel.setId(nextId);
                    FoodModel model = realm.copyToRealm(mahasiswaModel);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }

    public boolean isNotAvailable(int idMeals) {
        RealmResults<FoodModel> results = realm.where(FoodModel.class).equalTo("id_makanan", idMeals).findAll();
        return results.size() == 0;
    }

    // untuk memanggil semua data
    public List<FoodModel> getAllFood() {
        RealmResults<FoodModel> results = realm.where(FoodModel.class).findAll();
        return results;
    }

    // untuk menghapus data
    public void delete(String kolom, int idnya) {
        final RealmResults<FoodModel> results = realm.where(FoodModel.class).equalTo(kolom, idnya).findAll();
        results.size();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteFromRealm(0);
            }
        });
    }

}
