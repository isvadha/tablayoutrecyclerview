package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Rest;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Category;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Items;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("latest.php")
    Call<Items> getData();

    @GET("categories.php")
    Call<Category> getKategori();


    @GET("lookup.php")
    Call<Items> getDetail(@Query("i") int i);
}
