package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.Items;
import retrofit2.Response;

public class RecyclerFood extends RecyclerView.Adapter<RecyclerFood.ViewHolder> {
    ArrayList<Items.Meals> mealsArrayList;
    mClickListener mListener;

    public RecyclerFood(ArrayList<Items.Meals> mealsArrayList, mClickListener mListener) {
        this.mealsArrayList = mealsArrayList;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kiri_list_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.setData(mealsArrayList.get(i).getNamaMakanan(), mealsArrayList.get(i).getGambar());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.mOnClickListener(mealsArrayList.get(i).getIdMeal());
            }
        });
    }

    public interface mClickListener {
        void mOnClickListener(int id);
    }

    @Override
    public int getItemCount() {
        return mealsArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textview);
            imageView = itemView.findViewById(R.id.imageview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void setData(String title, String img) {
            textView.setText(title);
            Picasso.get().load(img).into(imageView);
        }
    }





//    static class ViewHolder extends RecyclerView.ViewHolder {
//        ImageView imageView;
//        TextView textView, areaView;
//
//
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//            imageView = itemView.findViewById(R.id.imageview);
//            textView = itemView.findViewById(R.id.namaMakanan);
//            areaView = itemView.findViewById(R.id.areaMakanan);
//            CardView cardView = itemView.findViewById(R.id.cvShow);
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
//                    /*itemView.getContext().startActivity(new Intent(itemView.getContext(),ShowDataDetail.class));*/
//                }
//            });
//            if (textView == null) {
//                Toast.makeText(itemView.getContext(), "Pastikan Jaringan Anda Lancar", Toast.LENGTH_SHORT).show();
//                cardView.setVisibility(View.GONE);
//            } else {
//                cardView.setVisibility(View.VISIBLE);
//            }
//        }
//
//        public void setData(String title, String img, String area) {
//            textView.setText(title);
//            areaView.setText(area);
//            Picasso.get().load(img).into(imageView);
//        }
//    }



}
