package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FoodModel extends RealmObject {

    @PrimaryKey
    private int id;
    private int id_makanan;
    private String nama_makanan;
    private String gambar_makanan;
    private String asal_negara;
    private String kategori;
    private String link_youtube;
    private String link_web;
    private String instruksi_pembuatan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_makanan() {
        return id_makanan;
    }

    public void setId_makanan(int id_makanan) {
        this.id_makanan = id_makanan;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getGambar_makanan() {
        return gambar_makanan;
    }

    public void setGambar_makanan(String gambar_makanan) {
        this.gambar_makanan = gambar_makanan;
    }

    public String getAsal_negara() {
        return asal_negara;
    }

    public void setAsal_negara(String asal_negara) {
        this.asal_negara = asal_negara;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getLink_youtube() {
        return link_youtube;
    }

    public void setLink_youtube(String link_youtube) {
        this.link_youtube = link_youtube;
    }

    public String getLink_web() {
        return link_web;
    }

    public void setLink_web(String link_web) {
        this.link_web = link_web;
    }

    public String getInstruksi_pembuatan() {
        return instruksi_pembuatan;
    }

    public void setInstruksi_pembuatan(String instruksi_pembuatan) {
        this.instruksi_pembuatan = instruksi_pembuatan;
    }
}
