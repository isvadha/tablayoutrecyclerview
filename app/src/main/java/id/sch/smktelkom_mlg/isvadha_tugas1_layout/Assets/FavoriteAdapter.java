package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.FoodModel;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    List<FoodModel> modelList;
    MyListener myListener;

    public FavoriteAdapter(List<FoodModel> foodModels, MyListener myListener) {
        this.modelList = foodModels;
        this.myListener = myListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.favorite_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(realmConfiguration);
        final RealmHelper realmHelper = new RealmHelper(realm);
        final int idMeals = modelList.get(position).getId_makanan();

        Picasso.get().load(modelList.get(position).getGambar_makanan()).into(viewHolder.imageViewFavorite);
        viewHolder.tvNamaMakananFavorite.setText(modelList.get(position).getNama_makanan());
        viewHolder.tvKategoriFavorite.setText(modelList.get(position).getKategori());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myListener.mOnClickListener(idMeals);
            }
        });
    }

    public interface MyListener {
        void mOnClickListener(int idMeals);

        void mOnClickListenerFavorite(FoodModel model);
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewFavorite;
        TextView tvNamaMakananFavorite, tvKategoriFavorite, tvHapus;
        CardView showDetailFavorite;

        public ViewHolder(@NonNull View v) {
            super(v);
            imageViewFavorite = v.findViewById(R.id.imageviewFavorite);
            tvNamaMakananFavorite = v.findViewById(R.id.namaMakananFavorite);
            tvKategoriFavorite = v.findViewById(R.id.categoriMakananFavorite);
            tvHapus = v.findViewById(R.id.deleteFromFavorite);
            showDetailFavorite = v.findViewById(R.id.cvFavorite);
        }
    }

}
