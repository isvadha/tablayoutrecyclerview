package id.sch.smktelkom_mlg.isvadha_tugas1_layout;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class InfoWindowCustom implements GoogleMap.InfoWindowAdapter  {

    Context context;

    public InfoWindowCustom(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View v = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.echo_info_window, null);

        TextView title = v.findViewById(R.id.info_window_title);
        ImageView imageView = v.findViewById(R.id.info_window_subtitle);

        title.setText(marker.getTitle());

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        imageView.setImageURI(Uri.parse(infoWindowData.getImage()));

        return v;
    }
}
