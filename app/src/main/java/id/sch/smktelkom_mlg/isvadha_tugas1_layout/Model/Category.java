package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Category {

    @SerializedName("categories")
    private ArrayList<categories> categorie;

    public ArrayList<categories> getCatogries() {
        return categorie;
    }

    public class categories {
        @SerializedName("strCategoryThumb")
        private String strCategoryThumb;
        @SerializedName("strCategory")
        private String strCategory;

        public String getStrCategoryThumb() {
            return strCategoryThumb;
        }

        public String getStrCategory() {
            return strCategory;
        }
    }

}
