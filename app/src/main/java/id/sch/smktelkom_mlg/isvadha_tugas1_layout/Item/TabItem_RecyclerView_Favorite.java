package id.sch.smktelkom_mlg.isvadha_tugas1_layout.Item;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.FavoriteAdapter;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Assets.RealmHelper;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.Model.FoodModel;
import id.sch.smktelkom_mlg.isvadha_tugas1_layout.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TabItem_RecyclerView_Favorite extends Fragment {

    RealmHelper realmHelper;
    RecyclerView rvFavorite;
    FavoriteAdapter.MyListener myListener;
    SwipeRefreshLayout swipeRefreshLayout;

    public TabItem_RecyclerView_Favorite() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.favorite, container, false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.myListener = (FavoriteAdapter.MyListener) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvFavorite = view.findViewById(R.id.recycler_view_favorite);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshFavorite);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFavorite();
            }
        });

        rvFavorite.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvFavorite.setLayoutManager(linearLayoutManager);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(realmConfiguration);
        realmHelper = new RealmHelper(realm);

        setAdapter();

    }

    private void refreshFavorite() {

        swipeRefreshLayout.setRefreshing(false);
        setAdapter();

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Stop animation (This will be after 3 seconds)
                swipeRefreshLayout.setRefreshing(false);
                setAdapter();
            }
        }, 2000);*/
    }

    private void setAdapter() {

        List<FoodModel> foodModels = realmHelper.getAllFood();
        FavoriteAdapter favoriteAdapter;
        favoriteAdapter = new FavoriteAdapter(foodModels, myListener);
        rvFavorite.setAdapter(favoriteAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        setAdapter();
    }
}
